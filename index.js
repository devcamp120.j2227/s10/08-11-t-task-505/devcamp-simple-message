// Khai báo thư viện ExpressJS
// import express from "express"
const express = require("express");

// Khai báo app express
const app = express();

// Khai báo cổng chạy API
const port = 8000;

// Cấu hình App đọc được body raw json
app.use(express.json());

let colors = ["Xanh", "Đỏ", "Tím", "Vàng"];

// Khai báo API GET /
app.get("/", (request, response) => {
    let today = new Date();

    response.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

// Khai báo API GET /get-method
app.get("/get-method", (request, response) => {
    response.json({
        colors: colors
    })
})

// Khai báo API POST /post-method
app.post("/post-method", (request, response) => {
    response.json({
        colors: [...colors, "Hồng"]
    })
})

// Khai báo API PUT /put-method
app.put("/put-method", (request, response) => {
    colors[2] = "Nâu";

    response.json({
        colors: colors
    })
})

// Khai báo API DELETE /delete-method
app.delete("/delete-method", (request, response) => {
    let [first_element, ...slice_array] = colors;

    response.json({
        colors: slice_array
    })
})

// Khai báo API GET Request params
app.get("/request-params/:colorIndex", (request, response) => {
    const index = request.params.colorIndex;
    
    response.json({
        color: colors[index]
    })
})

// Khai báo API GET Request query
app.get("/request-query", (request, response) => {
    const query = request.query;

    const color = query.color;

    let indexColor = colors.findIndex((element) => {
        return element == color
    })

    response.json({
        index: indexColor
    })
})

// Khai báo API POST Request Body JSON
app.post("/request-body-json", (request, response) => {
    const body = request.body;

    response.json({
        body: body
    })
})

// Chạy app express trên cổng 8000
app.listen(port, () => {
    console.log(`App đã được chạy trên cổng ${port}`);
})

